export const ADD_POST = "ADD_POST"
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS"
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS"


export const addPost = post => {
    let newPost = {
        author: post.author,
        content: post.content,
        title: post.title,
        createdAt: new Date().toLocaleString(),
        img_url: post.img_url,
        votes: 0,
        id: post.id
    }
    return dispatch => {
        dispatch({
            type: ADD_POST,
            payload: newPost
        })
    }
}

export const fetchPosts = () => {
    return dispatch => {
        fetch(`http://localhost:8082/api/posts`)
            .then(response => response.json())
            .then(response => dispatch({
                type: FETCH_POSTS_SUCCESS,
                payload: response
            }))
    }
}

export const fetchComments = () => {
    return dispatch => {
        fetch(`http://localhost:8082/api/comments`)
            .then(response => response.json())
            .then(response => dispatch({
                type: FETCH_COMMENTS_SUCCESS,
                payload: response
            }))
    }
}