import React from 'react'
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap'
import FaArrowUp from 'react-icons/lib/fa/arrow-up'
import FaArrowDown from 'react-icons/lib/fa/arrow-down'
import FaComment from 'react-icons/lib/fa/comment'


const Post = props => {

  return (
    <Row Row className="mt-3" >
      <Col>
        <Card>
          <img
            width="100%"
            src={props.img_url}
            alt="Card image cap"
          />
          <CardBody>
            <CardTitle>{props.title} | <FaArrowUp /> 1 <FaArrowDown /></CardTitle>
            <CardSubtitle>{props.author}</CardSubtitle>
            <CardText>
              {props.content}
            </CardText>
            <hr />
            {props.createdAt} | <FaComment /> 2 Comments
            <Form inline>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Input type="text" name="comment" id="comment-field" placeholder="Enter a comment here" />
              </FormGroup>
              <Button>Submit</Button>
            </Form>
            <ul className="mt-2">
              <li>Comment One</li>
              <li>Comment Two</li>
              <li>{props.content}</li>
            </ul>
          </CardBody>
        </Card>
      </Col>
    </Row >
  )
}

export default Post
