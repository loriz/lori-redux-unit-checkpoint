import React, { Component } from 'react'
import AddPostForm from './AddPostForm'
import Post from './Post'
import FilterPosts from './FilterPosts'
import { Container, Row, Col, Button } from 'reactstrap'
import { bindActionCreators } from 'redux'
import { fetchPosts, fetchComments } from '../actions'
import { connect } from 'react-redux'

class Main extends Component {

  componentDidMount() {
    this.props.fetchPosts(),
      this.props.fetchComments()
  }

  matchCommentsToPosts(postId) {
    let commentsArray = this.props.comments.filter(comment => comment.post_id === postId)
    return commentsArray;
  }

  render() {
    return (
      <Container className="mt-4">
        <Row>
          <Col sm={{ size: 8, offset: 1 }}>
            <FilterPosts />
          </Col>
          <Col sm="2">
            <Button color="secondary">Add Post</Button>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col sm={{ size: 11, offset: 1 }}>
            <AddPostForm />
          </Col>
        </Row>
        <Row>
          <Col className="pr-0" sm={{ size: 9, offset: 1 }}>
            {/* Below is the Post component for each post. It is up to you how you would like to iterate over them. */}
            {this.props.posts.map(post => <Post key={post.id} title={post.title} author={post.author} content={post.content} createdAt={post.createdAt} votes={post.votes} img_url={post.img_url} comments={matchCommentsToPosts(post.id)}/>)}       
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPosts,
  fetchComments
}, dispatch)

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
    comments: state.comments
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);