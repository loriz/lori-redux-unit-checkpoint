import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import reducers from './reducers'
import logger from 'redux-logger'


export default () => createStore(reducers, applyMiddleware(logger, thunkMiddleware))